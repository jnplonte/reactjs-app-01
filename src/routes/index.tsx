import React, { Suspense } from 'react';

import { Redirect, Switch } from 'react-router-dom';

import { LayoutRoute, UnAuthenticatedRoute } from '../components';
import { MinimalLayout } from '../layouts';

import { RadioView, NotFoundView } from '../views';

const Routes = () => {
	return (
		<Suspense fallback={<div></div>}>
			<Switch>
				<UnAuthenticatedRoute exact component={RadioView} layout={MinimalLayout} path="/" />
				<LayoutRoute component={NotFoundView} layout={MinimalLayout} path="/not-found" />
				<Redirect to="/not-found" />
			</Switch>
		</Suspense>
	);
};

export default Routes;
