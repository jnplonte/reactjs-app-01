import { lazy } from 'react';

export const RadioView = lazy(() => import('./radio/radio.view'));
export const NotFoundView = lazy(() => import('./not-found/not-found.view'));
