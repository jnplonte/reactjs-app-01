import { makeStyles } from '@material-ui/styles';

export const radioStyle = makeStyles((theme: any) => ({
	root: {
		backgroundColor: theme.palette.background.default,
		height: '100vh',
	},
	grid: {
		height: '100%',
	},
	name: {
		marginTop: theme.spacing(3),
		color: theme.palette.white,
	},
	content: {
		height: '100%',
		display: 'flex',
		flexDirection: 'column',
	},
	contentBody: {
		flexGrow: 1,
		display: 'flex',
		alignItems: 'center',
		[theme.breakpoints.down('md')]: {
			justifyContent: 'center',
		},
	},
	form: {
		paddingLeft: 100,
		paddingRight: 100,
		paddingBottom: 125,
		[theme.breakpoints.down('sm')]: {
			paddingLeft: theme.spacing(2),
			paddingRight: theme.spacing(2),
		},
	},
	title: {
		marginTop: theme.spacing(3),
	},
	leftText: {
		textAlign: 'left',
	},
	textField: {
		width: '100%',
		marginTop: theme.spacing(2),
	},
	okButton: {
		margin: theme.spacing(2, 0),
	},
}));
