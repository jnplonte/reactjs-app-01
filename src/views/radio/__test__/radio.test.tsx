import { render, screen, fireEvent } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { MockViews } from '../../../react-app-test-helper';

import { createMemoryHistory } from 'history';
import RadioView from '../radio.view';

describe('radio in view', () => {
	it('should render radio page', () => {
		const history = createMemoryHistory();
		const route = '/';
		history.push(route);

		render(<MockViews history={history} view={<RadioView />} />);

		const titleText = screen.getByText(/radio.radioTitle/i);

		const radio1 = screen.getByTestId(/radio0/i);
		const radio2 = screen.getByTestId(/radio1/i);
		const radio3 = screen.getByTestId(/radio2/i);

		expect(titleText).toBeInTheDocument();

		expect(radio1).toBeInTheDocument();
		expect(radio2).toBeInTheDocument();
		expect(radio3).toBeInTheDocument();
	});

	it('should select radio', async () => {
		const history = createMemoryHistory();
		const route = '/';
		history.push(route);

		render(<MockViews history={history} view={<RadioView />} />);

		const radio1Field: any = screen.getByTestId(/radio0/i).querySelector('input');
		await act(async () => {
			fireEvent.change(radio1Field, { target: { value: '103' } });
		});
		expect(radio1Field.value).toBe('103');

		const radio2Field: any = screen.getByTestId(/radio1/i).querySelector('input');
		await act(async () => {
			fireEvent.change(radio2Field, { target: { value: '206' } });
		});
		expect(radio2Field.value).toBe('206');

		const radio3Field: any = screen.getByTestId(/radio2/i).querySelector('input');
		await act(async () => {
			fireEvent.change(radio3Field, { target: { value: '304' } });
		});
		expect(radio3Field.value).toBe('304');

		const submitForm = screen.getByTestId(/okbutton/i);
		await act(async () => {
			fireEvent.submit(submitForm);
		});
	});
});
