import { ChangeEvent, FormEvent, useState, Fragment } from 'react';

import { withRouter } from 'react-router-dom';

import validateJS from 'validate.js';
import { Radio, RadioGroup } from '@mui/material';
import { Grid, Button, FormLabel, Typography, FormControl, FormControlLabel } from '@material-ui/core';

import { radioStyle } from './radio.style';

import { Helper } from '../../services/helper/helper.service';

import { IFormProps, menus, rules, schema, emptyForm } from './radio.constant';

const helper: Helper = new Helper();

const RadioView = (props: any) => {
	const classes: any = radioStyle();

	const [formState, setFormState] = useState<IFormProps>(emptyForm);

	const handleChange = (event: ChangeEvent<{ name?: string; value: unknown }> | null) => {
		event?.persist();

		const target: HTMLInputElement = event?.target as HTMLInputElement;

		setFormState((state: any) => {
			const updatedState = helper.initFormState(state, target);
			const updatedError = validateJS(updatedState.values, schema(), { fullMessages: false });

			return {
				...updatedState,
				isValid: updatedError ? false : true,
				errors: updatedError || {},
			};
		});
	};

	const handleSignIn = async (event: FormEvent | null) => {
		event?.preventDefault();

		if (formState.isValid) {
			console.log(formState.values, 'DATA HERE');
		}
	};

	return (
		<div className={classes.root}>
			<Grid className={classes.grid} container>
				<Grid className={classes.content} item xs={12}>
					<div className={classes.content}>
						<div className={classes.contentBody}>
							<form data-testid="submitform" className={classes.form} onSubmit={handleSignIn} noValidate>
								<Typography align="left" className={classes.title} variant="h4">
									{'radio.radioTitle'}
								</Typography>
								<FormControl component="fieldset" className={classes.textField}>
									{menus.map((menuData, menuIndx) => {
										return (
											<Fragment key={`menu-${menuIndx}`}>
												<FormLabel className={classes.leftText}>{`${menuIndx + 1}radio.form`}</FormLabel>
												<RadioGroup
													row
													data-testid={`radio${menuIndx}`}
													name={`radio${menuIndx}`}
													value={formState.values[`radio${menuIndx}`] || ''}
													onChange={handleChange}
												>
													{menuData.map((menuFieldData, menuFieldIndx) => {
														return (
															<FormControlLabel
																disabled={helper.hasFormError(formState, rules, `radio${menuIndx}`, menuFieldData.id)}
																key={`menu-field-${menuIndx}-${menuFieldIndx}`}
																value={menuFieldData.id}
																control={<Radio />}
																label={menuFieldData.value}
															/>
														);
													})}
												</RadioGroup>
											</Fragment>
										);
									})}
								</FormControl>
								<Button
									data-testid="okbutton"
									className={classes.okButton}
									disabled={!formState.isValid}
									color="primary"
									fullWidth
									size="large"
									type="submit"
									variant="contained"
								>
									{'radio.submit'}
								</Button>
							</form>
						</div>
					</div>
				</Grid>
			</Grid>
		</div>
	);
};

export default withRouter(RadioView);
