import React, { withRouter } from 'react-router-dom';

import { Grid, Typography } from '@material-ui/core';

import { notFoundStyle } from './not-found.style';

const NotFoundView = (props: any) => {
	const { history } = props;

	const classes: any = notFoundStyle();

	const handleGoBackToHome = () => {
		history.push('/');
	};

	return (
		<div className={classes.root}>
			<Grid container justifyContent="center" spacing={4}>
				<Grid item lg={6} xs={12}>
					<div className={classes.content}>
						<Typography variant="h1">{'notfound.pageNotFound'}</Typography>
						<Typography
							data-testid="goback"
							variant="subtitle1"
							onClick={handleGoBackToHome}
							className={classes.goback}
						>
							{'notfound.goBackToHome'}
						</Typography>
					</div>
				</Grid>
			</Grid>
		</div>
	);
};

export default withRouter(NotFoundView);
