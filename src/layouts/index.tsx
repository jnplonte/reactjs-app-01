import { lazy } from 'react';

export const MinimalLayout = lazy(() => import('./minimal/minimal.layout'));
