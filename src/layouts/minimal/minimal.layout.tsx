import * as PropTypes from 'prop-types';

import clsx from 'clsx';

import { minimalStyle } from './minimal.style';
import { Topbar } from './components';

const Minimal = (props: any) => {
	const { children } = props;

	const classes: any = minimalStyle();

	return (
		<div>
			<Topbar />
			<main className={clsx(classes.content, 'minimal-container')}>{children}</main>
		</div>
	);
};

Minimal.propTypes = {
	children: PropTypes.node,
};

export default Minimal;
