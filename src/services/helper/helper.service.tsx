import { toJson, toString, isNotEmpty, isEmpty, isEmptyObject } from 'jnpl-helper';

export class Helper {
	env: string = '';

	constructor() {
		this.env = process.env.NODE_ENV || '';
	}

	toJson(jsonData: any = ''): any {
		return toJson(jsonData);
	}

	toString(jsonData: any = ''): any {
		return toString(jsonData);
	}

	isNotEmpty(v: any = null): boolean {
		return isNotEmpty(v);
	}

	isEmpty(v: any = null): boolean {
		return isEmpty(v);
	}

	isEmptyObject(v: any = null): boolean {
		return isEmptyObject(v);
	}

	removeNullObject(obj: any = {}): object {
		for (const propName in obj) {
			if (obj[propName] === null || obj[propName] === undefined) {
				delete obj[propName];
			}
		}

		return obj || {};
	}

	hasFormError(formState: any, rules: any, field: string = '', value: string = ''): any {
		switch (field) {
			case 'radio0':
				return false;

			case 'radio1':
				if (this.isEmpty(formState['values']['radio0'])) {
					return true;
				}

				return rules[formState['values']['radio0']] && rules[formState['values']['radio0']].includes(value);
			case 'radio2':
				if (this.isEmpty(formState['values']['radio0']) || this.isEmpty(formState['values']['radio1'])) {
					return true;
				}

				return (
					(rules[formState['values']['radio0']] && rules[formState['values']['radio0']].includes(value)) ||
					(rules[formState['values']['radio1']] && rules[formState['values']['radio1']].includes(value))
				);
		}
	}

	initFormState(formState: any, target: any = {}): any {
		let targetVal: unknown;

		switch (target['type']) {
			case 'checkbox':
				targetVal = target['checked'];
				break;
			default:
				targetVal = target['value'];
				break;
		}

		return {
			...formState,
			values: {
				...formState['values'],
				[target['name']]: targetVal,
			},
			touched: {
				...formState.touched,
				[target['name']]: true,
			},
		};
	}

	initFormValue(target: any = {}): any {
		let targetVal: unknown;

		switch (target['type']) {
			case 'checkbox':
				targetVal = target['checked'];
				break;
			default:
				targetVal = target['value'];
				break;
		}

		return targetVal;
	}
}
