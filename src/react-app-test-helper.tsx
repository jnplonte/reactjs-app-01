import { Router } from 'react-router-dom';

import { ThemeProvider } from '@material-ui/core/styles';
import Theme from './themes';

const MockViews = (props: any) => {
	const { history, view } = props;

	return (
		<ThemeProvider theme={Theme}>
			<Router history={history}>{view}</Router>
		</ThemeProvider>
	);
};

export { MockViews };
