export interface IFormProps {
	isValid: boolean;
	disabledValues?: string[];
	values: any;
	touched?: object;
	errors?: object;
	gridFormat?: number[];
}

export interface ICoreProps {
	id?: string | number;
	name?: string;
}
